package json_ex_test

import (
	"go-example/json_ex"
	"testing"
)

func BenchmarkStdStrToMap(b *testing.B) {
	for i := 0; i < b.N; i++ {
		json_ex.StdStrToMap()
	}
}

func BenchmarkStdStrToStruct(b *testing.B) {
	for i := 0; i < b.N; i++ {
		json_ex.StdStrToStruct()
	}
}

func BenchmarkStdMapToStr(b *testing.B) {
	m := json_ex.JsonIteratorStrToMap()
	for i := 0; i < b.N; i++ {
		json_ex.StdMapToStr(m)
	}
}

func BenchmarkStdStructToStr(b *testing.B) {
	jsonOb := json_ex.JsonIteratorStrToStruct()
	for i := 0; i < b.N; i++ {
		json_ex.StdStructToStr(jsonOb)
	}
}

///////////////////////////////////////////////////////////////////////////////////////
func BenchmarkJsonIteratorStrToMap(b *testing.B) {
	for i := 0; i < b.N; i++ {
		json_ex.JsonIteratorStrToMap()
	}
}

func BenchmarkJsonIteratorStrToStruct(b *testing.B) {
	for i := 0; i < b.N; i++ {
		json_ex.JsonIteratorStrToStruct()
	}
}

func BenchmarkJsonIteratorMapToStr(b *testing.B) {
	m := json_ex.JsonIteratorStrToMap()
	for i := 0; i < b.N; i++ {
		json_ex.JsonIteratorMapToStr(m)
	}
}

func BenchmarkJsonIteratorStructToStr(b *testing.B) {
	jsonOb := json_ex.JsonIteratorStrToStruct()
	for i := 0; i < b.N; i++ {
		json_ex.JsonIteratorStructToStr(jsonOb)
	}
}
