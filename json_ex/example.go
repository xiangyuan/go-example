package json_ex

import (
	"encoding/json"
	"github.com/json-iterator/go"
)

var jsonStr = `
	{
  "person": {
    "name": {
      "first": "Leonid",
      "last": "Bugaev",
      "fullName": "Leonid Bugaev"
    },
    "github": {
      "handle": "buger",
      "followers": 109
    },
    "avatars": [
      { "url": "https://avatars1.githubusercontent.com/u/14009?v=3&s=460", "type": "thumbnail" }
    ]
  },
  "company": {
    "name": "Acme"
  }
}
`

type Name struct {
	First    string `json:"first"`
	Last     string `json:"last"`
	FullName string `json:"fullName"`
}
type Github struct {
	Handle    string `json:"handle"`
	Followers int    `json:"followers"`
}
type Avatars struct {
	Url  string `json:"url"`
	Type string `json:"type"`
}

type Person struct {
	Name    Name      `json:"name"`
	Github  Github    `json:"github"`
	Avatars []Avatars `json:"avatars"`
}

type Company struct {
	Name string `json:"name"`
}

type JsonOb struct {
	Person  Person  `json:"person"`
	Company Company `json:"company"`
}

//200000	      6517 ns/op
func StdStrToMap(jsonStr string) map[string]interface{} {
	m := make(map[string]interface{})
	json.Unmarshal([]byte(jsonStr), &m)
	return m
}

//200000	      6303 ns/op
func StdStrToStruct() *JsonOb {
	jsonOb := &JsonOb{}
	json.Unmarshal([]byte(jsonStr), &jsonOb)
	return jsonOb
}

//200000	      7091 ns/op
func StdMapToStr(m map[string]interface{}) {
	json.Marshal(m)
}

//1000000	      1492 ns/op
func StdStructToStr(jsonOb *JsonOb) {
	json.Marshal(jsonOb)
}

///////////////////////////////////////////////////////////////////////

//300000	      4299 ns/op
func JsonIteratorStrToMap() map[string]interface{} {
	m := make(map[string]interface{})
	jsoniter.Unmarshal([]byte(jsonStr), &m)
	return m
}

//1000000	      1529 ns/op
func JsonIteratorStrToStruct() *JsonOb {
	jsonOb := &JsonOb{}
	jsoniter.Unmarshal([]byte(jsonStr), &jsonOb)
	return jsonOb
}

//500000	      2883 ns/op
func JsonIteratorMapToStr(m map[string]interface{}) {
	jsoniter.Marshal(m)
}

//1000000	      1083 ns/op
func JsonIteratorStructToStr(jsonOb *JsonOb) {
	jsoniter.Marshal(jsonOb)
}
