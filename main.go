package main

import (
	"bufio"
	"encoding/base64"
	"fmt"
	"go-example/json_ex"
	"image"
	"image/jpeg"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
)

type words struct {
	words string `json:"words"`
}

type ocr struct {
	wordsResult []words `json:"words_result"`
}

func main() {
	base64, _ := Base64("D:\\12306img\\cc.jpg")
	ocr := Ocr(base64)
	fmt.Println(ocr)
	toMap := json_ex.StdStrToStruct(ocr)
	fmt.Println(toMap["words_result"].([]interface{})[0].(map[string]interface{})["words"])
}

func Base64(path string) (string, error) {
	imgFile, err := os.Open(path)
	if err != nil {
		log.Fatalln(err)
	}

	defer imgFile.Close()

	fInfo, _ := imgFile.Stat()
	var size int64 = fInfo.Size()
	buf := make([]byte, size)

	fReader := bufio.NewReader(imgFile)
	fReader.Read(buf)

	imgBase64str := base64.StdEncoding.EncodeToString(buf)
	return imgBase64str, nil
}

func CutTitle() {
	files, e := ListDir("D:\\12306img\\src")
	if e != nil {
		return
	}
	for _, file := range files {
		in, _ := os.Open(file)
		defer in.Close()
		image, _ := CutImage(in, 119, 0, 294, 29)
		out, _ := os.Create(strings.Replace(file, "src", "title", 1))
		defer out.Close()
		SaveToDevice(out, image)
	}
}

func ListDir(dir string) ([]string, error) {
	infos, e := ioutil.ReadDir(dir)
	if e != nil {
		return nil, e
	}
	var files []string
	for _, info := range infos {
		if !info.IsDir() {
			files = append(files, fmt.Sprintf("%s\\%s", dir, info.Name()))
		}
	}
	return files, nil
}

/**
 * 写入到硬盘
 */
func SaveToDevice(out io.Writer, img image.Image) {
	jpeg.Encode(out, img, &jpeg.Options{100})
}

/**
 * 切割图片
 */
func CutImage(inStream *os.File, x, y, w, h int) (image.Image, error) {
	img, _, _ := image.Decode(inStream)
	var subImg image.Image
	if rgbImg, ok := img.(*image.YCbCr); ok {
		subImg = rgbImg.SubImage(image.Rect(x, y, x+w, y+h)).(*image.YCbCr)
	} else if rgbImg, ok := img.(*image.RGBA); ok {
		subImg = rgbImg.SubImage(image.Rect(x, y, x+w, y+h)).(*image.RGBA)
	} else if rgbImg, ok := img.(*image.NRGBA); ok {
		subImg = rgbImg.SubImage(image.Rect(x, y, x+w, y+h)).(*image.NRGBA)
	}
	return subImg, nil
}

func Ocr(imageBase64 string) string {
	encodeurl := url.QueryEscape(imageBase64)
	url := "https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic?access_token=24.5fa6ba8ff261f256a85d13741450a09b.2592000.1539828061.282335-14219374"
	body := "image_type=BASE64&group_id=gropu001&user_id=0001&image=" + encodeurl

	resp, err := http.Post(url, "application/x-www-form-urlencoded", strings.NewReader(body))
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
	}
	return string(b)
}
