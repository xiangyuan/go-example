package rabbitmq_ex

import (
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"strconv"
	"time"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func Send() {
	conn, e := amqp.Dial("amqp://br:123456@192.168.1.142:5672/")
	if e != nil {
		failOnError(e, "Failed to connect to RabbitMQ")
	}
	defer conn.Close()
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	queue, err := ch.QueueDeclare("test_queue", false, false, false, false, nil)

	failOnError(err, "Failed to declare a queue")
	for i := 0; i < 10000; i++ {
		err = ch.Publish("", queue.Name, false, false, amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte("msg" + strconv.Itoa(i)),
		})
	}
	failOnError(err, "Failed to publish a message")
	fmt.Println("发送成功.")
	time.Sleep(time.Minute)
}
