package s_ex

import (
	"bytes"
	"math/rand"
	"strconv"
	"time"
)

var s string

func O() {
	for i := 0; i < 1; i++ {
		s += RandFloat()
	}
}

var ss bytes.Buffer

func OO() {
	for i := 0; i < 1; i++ {
		ss.WriteString(RandFloat())
	}
	s = ss.String()
}

/**
 * float64 随机数
 */
var r = rand.New(rand.NewSource(time.Now().UnixNano()))

func RandFloat() string {
	f := r.Float64()
	return strconv.FormatFloat(f, 'f', -1, 64)
}
