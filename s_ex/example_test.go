package s_ex_test

import (
	"go-example/s_ex"
	"testing"
)

//100	  90118244 ns/op
//200000  205887 ns/op
func BenchmarkO(b *testing.B) {
	for i := 0; i < b.N; i++ {
		s_ex.O()
	}
}

//5000	   4192152 ns/op
//5000	   4125533 ns/op
//200000   133175 ns/op
func BenchmarkOO(b *testing.B) {
	for i := 0; i < b.N; i++ {
		s_ex.OO()
	}
}
